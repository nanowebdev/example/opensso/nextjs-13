FROM node:16-alpine

WORKDIR /app/nextjs-13

COPY package*.json ./

RUN npm install

COPY . ./

RUN npm run build

EXPOSE 8000
