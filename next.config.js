/** @type {import('next').NextConfig} */
const nextConfig = {
  env: {
    baseUrl: 'http://localhost:8000',
  },
  images: {
    remotePatterns: [
      {
        protocol: "https",
        hostname: "**",
      },
    ],
  },
}

module.exports = nextConfig
