import { NextResponse } from 'next/server'
import { NextRequest } from 'next/server'
import jwt from './lib/jwt'

export const config = {
  matcher: [
    '/dashboard',
  ],
}

export async function middleware(request: NextRequest) {
  const cookies = await request.cookies
  if (cookies.has('token')) {
    const token = JSON.parse(JSON.stringify(cookies.get('token')))
    try {
      const verified = await jwt.verify(token.value)
      if (verified) return NextResponse.next()
    } catch (err) {
      console.log(err)
    }
  }
  return NextResponse.redirect(new URL('/logout', request.url))
}
