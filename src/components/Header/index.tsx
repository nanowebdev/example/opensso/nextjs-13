import Image from 'next/image'

export default function Header() {
  return (
    <div className='md:container md:mx-auto'>
      <Image
        className="relative dark:drop-shadow-[0_0_0.3rem_#ffffff70] dark:invert mt-4"
        src="/next.svg"
        alt="Next.js Logo"
        width={180}
        height={37}
        priority
      />
    </div>
  )
}
