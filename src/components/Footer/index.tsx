export default function Footer() {
  return (
    <footer className='md:container md:mx-auto mb-4'>
      <p>Copyright &copy; 2023</p>
    </footer>
  )
}
