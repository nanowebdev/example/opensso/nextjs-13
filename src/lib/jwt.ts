import * as jose from 'jose'

const alg = 'RS256'

const _jwtOptions = {
  maxTokenAge: '8h',
  algorithm: alg
}

async function _getPrivateKey() {
  return await fetch(
    new URL("../../private.key", import.meta.url)
  )
}

async function _getPublicKey() {
  return await fetch(
    new URL("../../public.key", import.meta.url)
  )
}

const sign = async (payload: jose.JWTPayload) => {
  const privateKey = await _getPrivateKey()
  const _privateKey = await privateKey.text()
  try {
    const key = await jose.importPKCS8(_privateKey, alg)
    const token = await new jose.SignJWT(payload)
      .setProtectedHeader({ alg })
      .setIssuedAt()
      .setExpirationTime(_jwtOptions.maxTokenAge)
      .sign(key)
    if (token) {
      return token
    }
    return false
  } catch (e) {
    return false
  }
}

const verify = async (token: string) => {
  const publicKey = await _getPublicKey()
  const _publicKey = await publicKey.text()
  try {
    const key = await jose.importSPKI(_publicKey, alg)
    const { payload } = await jose.jwtVerify(token, key, _jwtOptions)
    if (payload) {
      return payload
    }
    return false
  } catch (e) {
    return false
  }
}

const decode = (token: string) => {
  try {
    const payload = jose.decodeJwt(token)
    if (payload) {
      return payload
    }
    return false
  } catch (e) {
    return false
  }
}

const jwt = {
  verify,
  sign,
  decode
}

export default jwt
