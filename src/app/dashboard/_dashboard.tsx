'use client'
import Image from 'next/image'
import Button from '../../components/Button'
import { useEffect, useState } from 'react'

async function getSession() {
  const res = await fetch(process.env.baseUrl + '/api/session', { cache: 'no-store' })

  if (!res.ok) {
    // This will activate the closest `error.js` Error Boundary
    throw new Error('Failed to fetch data')
  }
  return await res.json()
}

export default function Dashboard() {
  const [session, setSession] = useState()

  useEffect(() => {
    getSession().then((data) => {
      setSession(data.payload)
    });
  }, [])

  function submitLogout() {
    location.href = 'logout'
  }

  if (!session) {
    return (
      <div className='md:container md:mx-auto mt-4 mb-4'>
        <div className="mt-52">
          <div className="grid place-items-center">
            <p>Loading...</p>
          </div>
        </div>
      </div>
    );
  }

  // convert session to object json
  const payload = JSON.parse(JSON.stringify(session))

  return (
    <div className='md:container md:mx-auto mt-4 mb-4'>
      <div className="mt-52">
        <div className="grid place-items-center">
          <div className="mt-2 mb-2">
            <Image
              className=""
              src={payload.gravatar}
              alt=""
              width={80}
              height={80}
              priority
            />
          </div>
          <table className="table-fixed">
            <tbody>
              <tr>
                <td>User ID: </td>
                <td>{payload.uid}</td>
              </tr>
              <tr>
                <td>Username: </td>
                <td>{payload.unm}</td>
              </tr>
              <tr>
                <td>Name: </td>
                <td>{payload.name}</td>
              </tr>
              <tr>
                <td>Email: </td>
                <td>{payload.mail}</td>
              </tr>
            </tbody>
          </table>
          <div className='mt-4'>
            <Button text='Logout' onClick={submitLogout} />
          </div>
        </div>
      </div>
    </div>
  )
}
