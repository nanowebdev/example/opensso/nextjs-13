'use client'
import { redirect } from 'next/navigation'
import Button from '../components/Button'

interface Props {
  searchParams: { [key: string]: string | string[] | undefined }
}

export default function Home({ searchParams }: Props) {
  const token = searchParams.token
  if (token) {
    return redirect('/authenticate/' + token)
  }

  function submitSSO() {
    location.href = "http://localhost:3000/sso/login/56bed89304f84f8ab756a437530024e9"
  }

  return (
    <>
      <div className='md:container md:mx-auto mt-4 mb-4'>
        <div className='mt-52 grid place-items-center'>
          <Button text='Sign in with Single Sign On' onClick={submitSSO} />
        </div>
      </div>
    </>
  )
}
