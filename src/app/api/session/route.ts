import jwt from '../../../lib/jwt'
import { NextResponse } from 'next/server'
import { cookies } from 'next/headers'

export async function GET(request: Request) {
  const token = JSON.parse(JSON.stringify(cookies().get('token')))
  if (token) {
    const payload = JSON.parse(JSON.stringify(await jwt.decode(token.value)))
    const data = { statusCode: 200, message: 'Successfully get user session', payload: payload }
    return NextResponse.json(data, { status: 200 })
  }
  return NextResponse.json({ statusCode: 200, message: 'Failed to get user session', payload: {} }, { status: 200 })
}
