import { cookies } from 'next/headers'
import { redirect } from 'next/navigation'

export async function GET(request: Request,
  { params }: { params: { slug: string } }) {
  // save token
  const workHours = 8 * 60 * 60 * 1000
  await cookies().set('token', params.slug, { expires: Date.now() + workHours })
  redirect('/dashboard')
}
